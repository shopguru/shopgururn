

export default{
	"relation":[
	{
		"LookupCode":"RELTN",
		"LookupValueCodeId":2,
		"LookupValueCode":"FATHR",
		"LookupValueText":"S/O|D/O",
		"ValueData":null,
		"UserName":null
	},
	{
		"LookupCode":"RELTN",
		"LookupValueCodeId":3,
		"LookupValueCode":"HUSBND",
		"LookupValueText":"H/O",
		"ValueData":null,
		"UserName":null
	},
	{
		"LookupCode":"RELTN",
		"LookupValueCodeId":4,
		"LookupValueCode":"UNCLE",
		"LookupValueText":"C/O",
		"ValueData":null,
		"UserName":null
	}
	]
}