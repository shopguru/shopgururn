
var domain = 'http://intrinsicindia.in/';
var customercontroller = 'Customer';
var mortgagecontroller = 'Mortgage';


var api = {
	quickAddCustomer(data){
		var url = domain+customercontroller+'/QuickAddNew';
		return fetch(url, {
  				method: 'POST',
  				headers: {
    			Accept: 'application/json',
    			'Content-Type': 'application/json',
    			'Access-Control-Allow-Origin':'*',
  				},
  				body: JSON.stringify({
  					"CustFName":data.fname,
					"CustLName":data.lname,
					"CustGenId":data.gender,
					"GardFName":data.gfname,
					"GardLName":data.glname,
					"CustRelationId":data.relation,
					"MobNo1":data.mobile,
					"AddressCodeId":data.city,
					"UserName":"Dummy"
  				}),
			});	
	},

	addCustomer(data){
		var url = domain+customercontroller+'/AddNew';
		return fetch(url, {
  				method: 'POST',
  				headers: {
    			Accept: 'application/json',
    			'Content-Type': 'application/json',
    			'Access-Control-Allow-Origin':'*',
  				},
  				body: JSON.stringify({
  					"CustFName":data.fname,
					"CustLName":data.lname,
					"CustGenId":data.gender,
					"GardFName":data.gfname,
					"GardLName":data.glname,
					"CustRelationId":data.relation,
					"MobNo1":data.mobile,
					"AddressCodeId":data.city,
					"MobNo2":data.contact,
					"IdentNo":data.idno,
					"CustIdentTypeId":data.idtype,
					"CustProfessionId":data.profession,
					"CustStsLupValId":'',
					"CustTypeLupValId":'',
					"CustAdd":data.address,
					"Remark":'',
					"CustEmailId":'',
					"UserName":"Dummy"
  				}),
			});	
	},

	findCustomer(data){
		var url = domain+customercontroller+'/GetMany';
		return fetch(url+'?CustCodeNo='+data.customercode+'&CustFName='+data.fname+'&CustLName='+data.lname+'&MobNo='+data.mobile);
	},
	findSingleCustomer(data){
		//console.log(data);
		var url = domain+customercontroller+'/GetDetail';
		return fetch(url+'?secCustCodeId='+data);
	},

	addNewMortgage(){
		var url = domain+mortgagecontroller+'/AddNew';
		return fetch(url, {
			method: 'POST',
			headers: {
		  Accept: 'application/json',
		  'Content-Type': 'application/json',
		  'Access-Control-Allow-Origin':'*',
			},
			body: JSON.stringify({
				"SecCustCodeId":'',
				"PrinAmt":'',
				"MrtgDate":'',
				"MnthIntRate":'',
				"PromisUnMrtgMnths":'',
				"Remark":'',
				"UserName":'Dummy',
				"MrtgItems":null,
			}),
	  	});
		

	},

	findMortgage(data){
		var url = domain+mortgagecontroller+'/GetMany';
		return fetch(url+'?CustCodeNo='+data.customercode+'&CustFName='+data.fname+'&CustLName='+data.lname+'&MobNo='+data.mobile);
	},

	findSingleMortgage(id1,id2){
		//console.log(data);
		var url = domain+mortgagecontroller+'/GetDetail';
		return fetch(url+'?secCustCodeId='+id1+'&secMrtgCodeId='+id2);
	},

}





module.exports = api;