
import React from 'react';
import { View, Text, TouchableOpacity, Button } from 'react-native';
import { Icon } from 'native-base';
import { StackNavigator } from 'react-navigation';
import MainTabNavigator from './MainTabNavigator';

const RootStackNavigator = StackNavigator(
  {
    Main: {
      screen: MainTabNavigator,
    },
  },
  {
    navigationOptions: ({navigation}) => ({
      headerStyle: {
            backgroundColor: '#222',
        },
      headerTitleStyle: {
        fontWeight: 'normal',
        marginLeft:0,
        color:'#eee',
      },
      title:'Shop Guru | Gupta Bhandar - Aap ki Dukaan',
      headerLeft: <View>
      <TouchableOpacity onPress={() => {
      if (navigation.state.index === 0) {
        navigation.navigate('DrawerOpen')
      } else {
        navigation.navigate('DrawerClose')
      }
    }}>
      <Icon name='md-menu' style={{marginLeft:15,width:50,color:'#fff'}} size={32}></Icon>
      </TouchableOpacity>
      </View>,
     headerRight: <View>
      <TouchableOpacity onPress={() => {
      // Coming soon: navigation.navigate('DrawerToggle')
      // https://github.com/react-community/react-navigation/pull/2492
      if (navigation.state.index === 0) {
        navigation.navigate('DrawerOpen')
      } else {
        navigation.navigate('DrawerClose')
      }
    }}>
      <Text style={{color:'#fff',marginRight:10}}>Username</Text>
      </TouchableOpacity>
      </View>,
     
    }),
  }
);

export default class RootNavigator extends React.Component {

  render() {
    return <RootStackNavigator />;
  }
}