import React from "react";
import { AppRegistry, Image, StatusBar,ScrollView,StyleSheet } from "react-native";
import Bar from 'react-native-bar-collapsible';

import {
  Button,
  Text,
  Header,
  Container,
  List,
  ListItem,
  Content,
  Icon,
  Footer
} from "native-base";

const routes1 = ["Find Customer", "Add Customer","Edit Customer","Quick Add Customer"];
const routes2 = ["Add Mortgage","Find Mortgage","View Mortgage","Update Mortgage", "Find Unsecure Mortgage"];
const routes3 = ["Find Lending"];
const routes4 = ["Add Village/City", "Register New User"];



export default class SideBar extends React.Component {

  
  

  render() {

   

    return (
      <Container >
      <ScrollView style={styles.scrollview}>
        <Content>
          <Image
            source={{
              uri: "http://4.bp.blogspot.com/-dcedHw8-iKE/UZvmNiv94WI/AAAAAAAAuGg/ZBChAAlP-uc/s1600/shopguru-logo-01.jpg"
            }}
            style={{
              height: 120,
              alignSelf: "stretch",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
          
            
          </Image>
          <Bar
              title='Manage Customer'
              titleStyle={{ color: '#fff',fontSize:18 }}
              collapsible={true}
              showOnStart={false}
              iconCollapsed='plus'
              iconOpened='minus'
              iconSize={15}
              style={styles.bar}
          >
            <List
            dataArray={routes1}
            renderRow={data => {
              return (
                <ListItem
                  button
                  onPress={() => this.props.navigation.navigate(data)}
                  style={styles.li}
                >
                  <Text style={{color:'#fff',paddingLeft:15}}> - {data}</Text>
                </ListItem>
              );
            }}
          />
          </Bar>

          <Bar
              title='Manage Mortgage'
              titleStyle={{ color: '#fff',fontSize:18 }}
              collapsible={true}
              showOnStart={false}
              iconCollapsed='plus'
              iconOpened='minus'
              iconSize={15}
              style={styles.bar}
          >
            <List
            dataArray={routes2}
            renderRow={data => {
              return (
                <ListItem
                  button
                  onPress={() => this.props.navigation.navigate(data)}
                  style={styles.li}
                >
                  <Text style={{color:'#fff',paddingLeft:15}}> - {data}</Text>
                </ListItem>
              );
            }}
          />
          </Bar>

          <Bar
              title='Manage Lending'
              titleStyle={{ color: '#fff',fontSize:18 }}
              collapsible={true}
              showOnStart={false}
              iconCollapsed='plus'
              iconOpened='minus'
              iconSize={15}
              style={styles.bar}
          >
            <List
            dataArray={routes3}
            renderRow={data => {
              return (
                <ListItem
                  button
                  onPress={() => this.props.navigation.navigate(data)}
                  style={styles.li}
                >
                  <Text style={{color:'#fff',paddingLeft:15}}> - {data}</Text>
                </ListItem>
              );
            }}
          />
          </Bar>

          <Bar
              title='Configs'
              titleStyle={{ color: '#fff',fontSize:18 }}
              collapsible={true}
              showOnStart={false}
              iconCollapsed='plus'
              iconOpened='minus'
              iconSize={15}
              style={styles.bar}
          >
            <List
            dataArray={routes4}
            renderRow={data => {
              return (
                <ListItem
                  button
                  onPress={() => this.props.navigation.navigate(data)}
                  style={styles.li}
                >
                  <Text style={{color:'#fff',paddingLeft:15}}> - {data}</Text>
                </ListItem>
              );
            }}
          />
          </Bar>
          
        </Content>
        
      
      </ScrollView>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  scrollview:{
    backgroundColor:'#313740',
    flex:1
  },
  bar:{backgroundColor:'#505A6A',
  height:50,
  borderBottomWidth:0.5,
  borderBottomColor:'#777'
  },
  li:{backgroundColor:'#313740',marginLeft:0},
});

