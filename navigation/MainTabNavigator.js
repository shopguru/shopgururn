import React from 'react';
import PropTypes from 'prop-types';
import { DrawerNavigator } from 'react-navigation';
import SideBar from './SideBar/SideBar.js';
import Colors from '../constants/Colors';

import FindCustomer from '../screens/managecustomer/FindCustomer';
import ViewCustomer from '../screens/managecustomer/ViewCustomer';
import AddCustomer from '../screens/managecustomer/AddCustomer';
import EditCustomer from '../screens/managecustomer/EditCustomer';
import QuickAddCustomer from '../screens/managecustomer/QuickAddCustomer';
import QuickAddSuccessscreen from '../screens/managecustomer/QuickAddSuccess';
import AddSuccessscreen from '../screens/managecustomer/QuickAddSuccess';

import AddMortgage from '../screens/managemortgage/AddMortgage';
import FindMortgage from '../screens/managemortgage/FindMortgage';
import ViewMortgage from '../screens/managemortgage/ViewMortgage';
import UpdateMortgage from '../screens/managemortgage/UpdateMortgage';


export default DrawerNavigator(
  {
    'Find Customer': {
      screen: FindCustomer,
    },
    'View Customer': {
      screen: ViewCustomer,
    },
    'Add Customer': {
      screen: AddCustomer,
    },
    'Add Customer Success': {
      screen: AddSuccessscreen,
    },
    'Edit Customer': {
      screen: EditCustomer,
    },
    'Quick Add Customer': {
      screen: QuickAddCustomer,
    },
    'Quick Add Success':{
      screen: QuickAddSuccessscreen,
    },
    'Add Mortgage':{
      screen: AddMortgage,
    },
    'Find Mortgage':{
      screen: FindMortgage,
    },
    'View Mortgage':{
      screen: ViewMortgage,
    },
    'Update Mortgage':{
      screen: UpdateMortgage,
    },
    
  },
  {
    contentComponent: props => <SideBar {...props} />
  });

