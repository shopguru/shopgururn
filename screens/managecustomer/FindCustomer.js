import React from 'react';
import PropTypes from 'prop-types';
import { Platform, ScrollView, StyleSheet, Text,Dimensions,View,Note } from 'react-native';
import { Container, Header, Content, Form, Item,Body,Spinner, Input, Label,Button,List,ListItem,Right,Left,Radio,Picker } from 'native-base';
import api from '../../utils/api.js';
import cv from '../../utils/CityVillages.json';

export default class FindCustomer extends React.Component {
  static navigationOptions = {
    title: 'Find Customer',
  };

  findcustomer(){
    this.setState({showspinner:true});
    api.findCustomer(this.state)
    .then((res) => res.json()
    .then((responseJson) => {
      console.log(responseJson);
      if(responseJson.Status == 1){
        this.setState({listofcustomers:responseJson.ReturnData});
        this.setState({showspinner:false});
        this.setState({showresults:true});
      }
      else{alert("Error! Something went wrong");}
    })
    );
  }

  viewcustomer(value){
    api.findSingleCustomer(value)
    .then((res) => res.json()
    .then((responseJson) => {
      if(responseJson.Status == 1){this.gotocustomer(responseJson);}
      else{alert("Error! Something went wrong");}
    })
    );
  }
  
  gotocustomer(responseJson){
    this.props.navigation.navigate("View Customer",responseJson);
  }

   constructor(){
    super();
    this.state = {
      fname:'',
      lname:'',
      mobile:'',
      city: '',
      customercode:'',
      listofcustomers:[],
      showspinner:false,
      showresults : false,
      cvdata:cv,
    }
  }
  reset(){
    this.setState({
      fname:'',
      lname:'',
      mobile:'',
      city: '',
      customercode:'',
      listofcustomers:[],
      showspinner:false,
      showresults : false,
      cvdata:cv,
    });
  }
  
 citychange(value: string) {
    this.setState({
      city: value
    });
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <Content style={styles.section}>
          <Text style={{fontWeight:'600',margin:15,fontSize:18,color:'#777'}}>
            Find Customer
          </Text>
          <Form>
            <Item stackedLabel>
              <Label>First Name</Label>
              <Input onChangeText={(text) => this.setState({fname: text})} value={this.state.fname}/>
            </Item>
            <Item stackedLabel>
              <Label>Last Name</Label>
              <Input onChangeText={(text) => this.setState({lname: text})} value={this.state.lname}/>
            </Item>
            <Item stackedLabel>
              <Label>Mobile Number</Label>
              <Input onChangeText={(text) => this.setState({mobile: text})} value={this.state.mobile}/>
            </Item>
            <Item stackedLabel>
              <Label>Village/City</Label>
              <Picker style={{ width:(Platform.OS === 'ios') ? undefined : '100%',flexDirection:'row',alignItems:'flex-start' }}
                mode="dropdown"
                placeholder="Select One"
                selectedValue={this.state.city}
                onValueChange={this.citychange.bind(this)}
              >
                {this.state.cvdata.map((v,i)=>{
                return <Item key={i} label={v.ItemName} value={v.ItemValueId} />
              })}
              </Picker>
            </Item>

            <Item stackedLabel>
              <Label>Customer Code</Label>
              <Input onChangeText={(text) => this.setState({customercode: text})} value={this.state.customercode}/>
            </Item>
            
            <View style={{flexDirection:'row',marginTop:10,marginLeft:15}}>
            <Button primary block style={{flex:1,marginRight:5}} onPress={this.findcustomer.bind(this)}><Text> Find Customer </Text></Button>
            <Button light block style={{flex:1,marginLeft:5}} onPress={this.reset.bind(this)}><Text> Clear </Text></Button>
            </View>
          </Form>
        </Content>


        {this.state.showspinner && <Spinner color='black'/>}

        {this.state.showresults && <Content style={styles.section}>
          <Text style={{fontWeight:'600',margin:15,fontSize:18,color:'#777'}}>
            Results
          </Text>
          <List
            dataArray={this.state.listofcustomers}
            renderRow={data => {
              return (
                <ListItem>
                <Body>
                  <Text>{data.CustFName} {data.CustLName}</Text>
                  <Text>S/O {data.GardFName} {data.GardLName}</Text>
                  <Text>{data.AddressName}</Text>
                </Body>
                <Right>
                  <Button small bordered onPress={this.viewcustomer.bind(this,data.SecCustCodeId)}><Text>  View  </Text></Button>
                </Right>
                  
                </ListItem>
              );
            }}
          />
        </Content>
      }
      </ScrollView>


    )
    
  
    
  }

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2',
  },
  section:{
    margin:10,
    backgroundColor:'#fff',
    flex:1,
    paddingBottom:10,
    paddingRight:15,
  },
});

