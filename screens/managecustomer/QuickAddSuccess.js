import React from 'react';
import PropTypes from 'prop-types';
import { Platform, ScrollView, StyleSheet, Text,Dimensions,View,AsyncStorage } from 'react-native';
import { Container, Header, Content, Form, Item, Input, Label,Button,List,ListItem,Right,Left,Radio,Picker,Icon } from 'native-base';
import api from '../../utils/api.js';
import settingsObj from '../../utils/lookup.js'; 



export default class QuickAddSuccess extends React.Component {
  static navigationOptions = {
    title: 'Quick Add Success',
  };
  
  goback(){
    this.props.navigation.navigate("Quick Add Customer");
  }  

render() {
    return (
      
      <Container style={styles.container}>
        
        <Content style={styles.section}>
          
          <View style={{alignItems:'center',marginTop:50,marginBottom:50}}>
          <Icon name="md-checkmark-circle" size={32} color="#777" />
          <Text>Customer Added Successfully</Text>
          </View>
          <View>
          <Button block style={styles.btn}><Text>Add Mortgage</Text></Button>
          <Button block style={styles.btn}><Text>Add Borrow</Text></Button>
          <Button block style={styles.btn}><Text>Add Unsecure Mortgage</Text></Button>
          <Button block success style={styles.btn} onPress={this.goback.bind(this)}><Text>Done</Text></Button>
          </View>
          
        </Content>
        
      </Container>
      
        
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2',
  },
  section:{
    margin:10,
    backgroundColor:'#fff',
    flex:1,
    paddingBottom:10,
    paddingRight:15,
    flexDirection:'column',

  },
  btn:{
    marginBottom:5,
    marginLeft:15,
  },
});

