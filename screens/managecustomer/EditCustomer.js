import React from 'react';
import PropTypes from 'prop-types';
import { Platform, ScrollView, StyleSheet, Text,Dimensions,View } from 'react-native';
import { Container, Header, Content, Form, Item, Input, Label,Button,List,ListItem,Right,Left,Radio,Picker } from 'native-base';
import cv from '../../utils/CityVillages.json';
import district from '../../utils/Districts.json';
import states from '../../utils/States.json';
import lookupvalues from '../../utils/LookupValues.json';
import api from '../../utils/api.js';


export default class EditCustomer extends React.Component {
  static navigationOptions = {
     title: 'Edit Customer',
  };
  constructor(){
    super();
    this.state = {
      gender:0,
      relation:undefined,
      city:undefined,
      profession:undefined,
      idtype:undefined,
      district:undefined,
      mystate:undefined,
      contact:undefined,
      cvdata:cv,
      lookupdata:lookupvalues,
      distdata:district,
      statesdata:states,
      userdata:null,
    }
  }

  componentWillMount(){
    console.log(this.props.navigation.state.params);
    this.setState({userdata : this.props.navigation.state.params});
  }

  relationchange(value: string) {
    this.setState({
      relation: value
    });
  }
  citychange(value: string) {
    this.setState({
      city: value
    });
  }
  professionchange(value: string) {
    this.setState({
      profession: value
    });
  }
  idtypechange(value: string) {
    this.setState({
      idtype: value
    });
  }
  districtchange(value: string) {
    this.setState({
      district: value
    });
  }
  statechange(value: string) {
    this.setState({
      mystate: value
    });
  }

  render() {

    var reloptions = this.state.lookupdata.filter(function(item){
        return item.LookupTableCode=='RELTN';
    })
    var profoptions = this.state.lookupdata.filter(function(item){
        return item.LookupTableCode=='PROFSION';
    })
    var idtypeoptions = this.state.lookupdata.filter(function(item){
        return item.LookupTableCode=='IDENTTYP';
    })

    return (
      <ScrollView style={styles.container}>
        <Content style={styles.section}>
          <Text style={{fontWeight:'600',margin:15,fontSize:18,color:'#777'}}>
            Edit Customer
          </Text>
          <Text>{JSON.stringify(this.state.userdata)}</Text>
          <Form>
            <Item stackedLabel>
              <Label>First Name</Label>
              <Input />
            </Item>
            <Item stackedLabel>
              <Label>Last Name</Label>
              <Input />
            </Item>

            
            <Item stackedLabel style={{flex:1,position:'relative',alignItems:'flex-start',}}>
              <Label>Gender</Label>
              <View style={{flex:1,flexDirection:'row',alignItems:'flex-start',marginTop:20,marginBottom:10}}>
              <Radio selected={this.state.gender == 0} onPress={() => this.setState({ gender: 0 })}/>
              <Text> Male</Text>
              <Radio selected={this.state.gender == 1} style={{marginLeft:30}} onPress={() => this.setState({ gender: 1 })}/>
              <Text> Female</Text>
              </View>
            </Item>
            
            
            <Item stackedLabel>
              <Label>Mobile Number</Label>
              <Input />
            </Item>
            <Item stackedLabel>
              <Label>Guardian First Name</Label>
              <Input />
            </Item>
            <Item stackedLabel>
              <Label>Guardian Last Name</Label>
              <Input />
            </Item>
            
            <Item stackedLabel>
              <Label>Relation</Label>
              <Picker style={{ width:(Platform.OS === 'ios') ? undefined : '100%',flexDirection:'row',alignItems:'flex-start' }}
                mode="dropdown"
                placeholder="Select One"
                selectedValue={this.state.relation}
                onValueChange={this.relationchange.bind(this)}
              >
                {reloptions.map((v,i)=>{
                return <Item key={i} label={v.LookupValueText} value={v.LookupValueCodeId} />
              })}
        
              </Picker>
            </Item>

            <Item stackedLabel>
              <Label>Profession</Label>
              <Picker style={{ width:(Platform.OS === 'ios') ? undefined : '100%',flexDirection:'row',alignItems:'flex-start' }}
                mode="dropdown"
                placeholder="Select One"
                selectedValue={this.state.profession}
                onValueChange={this.professionchange.bind(this)}
              >
                {profoptions.map((v,i)=>{
                return <Item key={i} label={v.LookupValueText} value={v.LookupValueCodeId} />
              })}
              </Picker>
            </Item>

             <Item stackedLabel>
              <Label>Identity Number</Label>
              <Input onChangeText={(text) => this.setState({idno: text})} value={this.state.idno}/>
            </Item>

            <Item stackedLabel>
              <Label>Identity Type</Label>
              <Picker style={{ width:(Platform.OS === 'ios') ? undefined : '100%',flexDirection:'row',alignItems:'flex-start' }}
                mode="dropdown"
                placeholder="Select One"
                selectedValue={this.state.idtype}
                onValueChange={this.idtypechange.bind(this)}
              >
                {idtypeoptions.map((v,i)=>{
                return <Item key={i} label={v.LookupValueText} value={v.LookupValueCodeId} />
              })}
              </Picker>
            </Item>
            
            <Item stackedLabel>
              <Label>Village/City</Label>
              <Picker style={{ width:(Platform.OS === 'ios') ? undefined : '100%',flexDirection:'row',alignItems:'flex-start' }}
                mode="dropdown"
                placeholder="Select One"
                selectedValue={this.state.city}
                onValueChange={this.citychange.bind(this)}
              >
                 {this.state.cvdata.map((v,i)=>{
                return <Item key={i} label={v.ItemName} value={v.ItemValueId} />
              })}
              </Picker>
            </Item>

            <Item stackedLabel>
              <Label>District</Label>
              <Picker style={{ width:(Platform.OS === 'ios') ? undefined : '100%',flexDirection:'row',alignItems:'flex-start' }}
                mode="dropdown"
                placeholder="Select One"
                selectedValue={this.state.district}
                onValueChange={this.districtchange.bind(this)}
              >
                {this.state.distdata.map((v,i)=>{
                return <Item key={i} label={v.ItemName} value={v.ItemValueId} />
              })}
              </Picker>
            </Item>

            <Item stackedLabel>
              <Label>Address</Label>
              <Input onChangeText={(text) => this.setState({address: text})} value={this.state.address}/>
            </Item>

            <Item stackedLabel>
              <Label>State</Label>
              <Picker style={{ width:(Platform.OS === 'ios') ? undefined : '100%',flexDirection:'row',alignItems:'flex-start' }}
                mode="dropdown"
                placeholder="Select One"
                selectedValue={this.state.mystate}
                onValueChange={this.statechange.bind(this)}
              >
                {this.state.statesdata.map((v,i)=>{
                return <Item key={i} label={v.ItemName} value={v.ItemValueId} />
              })}
              </Picker>
            </Item>

            <Item stackedLabel>
              <Label>Contact Number</Label>
              <Input onChangeText={(text) => this.setState({contact: text})} value={this.state.contact}/>
            </Item>

            <View style={{flexDirection:'row',marginTop:10,marginLeft:15}}>
            <Button primary block style={{flex:1,marginRight:5}}><Text> Save </Text></Button>
            <Button light block style={{flex:1,marginLeft:5}}><Text> Cancel </Text></Button>
            </View>
          </Form>
        </Content>
      </ScrollView>
    );
  }
}

  
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2',
  },
  section:{
    margin:10,
    backgroundColor:'#fff',
    flex:1,
    paddingBottom:10,
    paddingRight:15,
  },
});
