import React from 'react';
import PropTypes from 'prop-types';
import { Platform, ScrollView, StyleSheet, Text,Dimensions,View,Note } from 'react-native';
import { Container, Header, Content, Form, Item,Body,Spinner, Input, Label,Button,List,ListItem,Right,Left,Radio,Picker } from 'native-base';
import api from '../../utils/api.js';

export default class ViewCustomer extends React.Component {
  static navigationOptions = {
    title: 'View Customer',
  };

  constructor(){
    super();
    this.state = {
      userdata : null, 
    }
  }

  componentWillMount(){
    console.log(this.props.navigation.state.params.ReturnData);
    this.setState({userdata : this.props.navigation.state.params.ReturnData});
  }

  editcustomer(editdata){
    this.props.navigation.navigate("Edit Customer",editdata);
  }

  render() {
    return (

      <Container style={styles.container}>
      <Text>{JSON.stringify(this.state.userdata)}</Text>
        <Content style={styles.section}>
          
          <View style={{marginBottom:20}}>
          <Text style={{fontWeight:'600',margin:15,fontSize:18,color:'#777'}}>
            View Customer
          </Text>
            <List>
            <ListItem>
                  <Body>
                   
                    <Text>Name : {this.state.userdata.CustFName} {this.state.userdata.CustLName} </Text>
                  </Body>
            </ListItem>
            <ListItem>
                  <Body>
                    
                    <Text>Guardian Name : {this.state.userdata.GardFName} {this.state.userdata.GardLName}</Text>
                  </Body>
            </ListItem>
            <ListItem>
                  <Body>
                    
                    <Text>Customer Code : {this.state.userdata.CustCodeNo}</Text>
                  </Body>
            </ListItem>
            <ListItem>
                  <Body>
                    
                    <Text>Mobile : {this.state.userdata.MobNo1}</Text>
                  </Body>
            </ListItem>
            <ListItem>
                  <Body>
                    
                    <Text>Address : {this.state.userdata.Address}</Text>
                  </Body>
            </ListItem>
            </List>
          </View>
          <View>
          <Button block style={styles.btn}><Text>View Transaction Snap</Text></Button>
          <Button block style={styles.btn}><Text>Add Mortgage</Text></Button>
          <Button block style={styles.btn}><Text>Add Borrow</Text></Button>
          <Button block style={styles.btn}><Text>Add Unsecure Mortgage</Text></Button>
          <Button block success style={styles.btn} onPress={this.editcustomer.bind(this,this.state.userdata)}><Text>Edit Details</Text></Button>
          </View>
          
        </Content>
        
      </Container>


    )
    
  
    
  }

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2',
  },
  section:{
    margin:10,
    backgroundColor:'#fff',
    flex:1,
    paddingBottom:10,
    paddingRight:15,
    flexDirection:'column',

  },
  btn:{
    marginBottom:5,
    marginLeft:15,
  },
});

