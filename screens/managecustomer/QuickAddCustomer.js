import React from 'react';
import PropTypes from 'prop-types';
import { Platform, ScrollView, StyleSheet, Text,Dimensions,View } from 'react-native';
import { Container, Header, Content, Form, Item, Input, Label,Button,List,ListItem,Right,Left,Radio,Picker } from 'native-base';
import api from '../../utils/api.js';
import lookup from '../../utils/lookup.js';
import cv from '../../utils/CityVillages.json';
import lookupvalues from '../../utils/LookupValues.json';


export default class QuickAddCustomer extends React.Component {
  static navigationOptions = {
    title: 'Quick Add Customer',
  };

  quickaddcustomer(){
    api.quickAddCustomer(this.state)
    .then((res) => res.json()
    .then((responseJson) => {
      if(responseJson.Status == 1){this.showsuccess();}
      else{alert("Error! Something went wrong");}
    })
    );
  }

  reset(){
    this.setState({
      fname : '',
      lname : '',
      mobile : '',
      gfname:'',
      glname:'',
      gender:5,
      relation:undefined,
      city:undefined,
    })
  }

  constructor(){
    super();
    this.state = {
      fname : '',
      lname : '',
      mobile : '',
      gfname:'',
      glname:'',
      gender:5,
      relation:undefined,
      city:undefined,
      lookupdata:lookupvalues,
      cvdata:cv,
    }
  }
  
  relationchange(value: string) {
    this.setState({
      relation: value
    });
  }
  
  showsuccess(){
    this.props.navigation.navigate("Quick Add Success");
  }
  
  citychange(value: string) {
    this.setState({
      city: value
    });
  }

  


  
  
render() {
    
    var reloptions = this.state.lookupdata.filter(function(item){
        return item.LookupTableCode=='RELTN';
    })

    return (
      <ScrollView style={styles.container}>
        <Content style={styles.section}>
          <Text style={{fontWeight:'600',margin:15,fontSize:18,color:'#777'}}>
            Quick Add Customer
          </Text>
          <Form>
            <Item stackedLabel>
              <Label>First Name</Label>
              <Input onChangeText={(text) => this.setState({fname: text})} value={this.state.fname}/>
            </Item>
            <Item stackedLabel>
              <Label>Last Name</Label>
              <Input onChangeText={(text) => this.setState({lname: text})} value={this.state.lname}/>
            </Item>

            
            <Item stackedLabel style={{flex:1,position:'relative',alignItems:'flex-start',}}>
              <Label>Gender</Label>
              <View style={{flex:1,flexDirection:'row',alignItems:'flex-start',marginTop:20,marginBottom:10}}>
              <Radio selected={this.state.gender == 5} onPress={() => this.setState({ gender: 5 })}/>
              <Text> Male</Text>
              <Radio selected={this.state.gender == 6} style={{marginLeft:30}} onPress={() => this.setState({ gender: 6 })}/>
              <Text> Female</Text>
              </View>
            </Item>
            
            
            <Item stackedLabel>
              <Label>Mobile Number</Label>
              <Input onChangeText={(text) => this.setState({mobile: text})} value={this.state.mobile}/>
            </Item>
            <Item stackedLabel>
              <Label>Guardian First Name</Label>
              <Input onChangeText={(text) => this.setState({gfname: text})} value={this.state.gfname}/>
            </Item>
            <Item stackedLabel>
              <Label>Guardian Last Name</Label>
              <Input onChangeText={(text) => this.setState({glname: text})} value={this.state.glname}/>
            </Item>
            
            <Item stackedLabel>
              <Label>Relation</Label>
              <Picker style={{ width:(Platform.OS === 'ios') ? undefined : '100%',flexDirection:'row',alignItems:'flex-start' }}
                mode="dropdown"
                placeholder="Select One"
                selectedValue={this.state.relation}
                onValueChange={this.relationchange.bind(this)}
              >
              {reloptions.map((v,i)=>{
                return <Item key={i} label={v.LookupValueText} value={v.LookupValueCodeId} />
              })}
              </Picker>
            </Item>
            
            <Item stackedLabel>
              <Label>Village/City</Label>
              <Picker style={{ width:(Platform.OS === 'ios') ? undefined : '100%',flexDirection:'row',alignItems:'flex-start' }}
                mode="dropdown"
                placeholder="Select One"
                selectedValue={this.state.city}
                onValueChange={this.citychange.bind(this)}
              >
              {this.state.cvdata.map((v,i)=>{
                return <Item key={i} label={v.ItemName} value={v.ItemValueId} />
              })}
              </Picker>
            </Item>

            <View style={{flexDirection:'row',marginTop:10,marginLeft:15}}>
            <Button primary block style={{flex:1,marginRight:5}} onPress={this.quickaddcustomer.bind(this)}><Text> Save </Text></Button>
            <Button light block style={{flex:1,marginLeft:5}} onPress={this.reset.bind(this)}><Text> Cancel </Text></Button>
            </View>
          </Form>
        </Content>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2',
  },
  section:{
    margin:10,
    backgroundColor:'#fff',
    flex:1,
    paddingBottom:10,
    paddingRight:15,
  },
});
