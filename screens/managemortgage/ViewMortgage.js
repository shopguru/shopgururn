import React from 'react';
import { Platform, ScrollView, StyleSheet, Text,Dimensions,View,Note } from 'react-native';
import { Container, Header, Content, Form, Item,Body,Spinner, Input, Label,Button,Badge,List,ListItem,Right,Left,Radio,Picker } from 'native-base';
import api from '../../utils/api.js';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';


export default class ViewMortgage extends React.Component {
  static navigationOptions = {
    title: 'View Mortgage',
  };

  constructor(){
    super();
    this.state = {
      userdata : null, 
    }
  }


  componentWillMount(){
    this.setState({userdata : this.props.navigation.state.params.ReturnData});
    

    // this.state.userdata.Mortgage.map(function(item) {
    //   console.warn(item);
    
    // });
    // // var dt = this.state.userdata;
    // foreach(dt.Mortgage as data){
    //   const table1data = [data.TRNo,data.PrinAmt,data.MrtgDate,data.PromisUnMrtgMnths,data.MrtgSts,data.Remark]
    // }
  }

  render() {

    const tableHead = ['TRNo','Principal','Mortgage Date','Promised Month','Mortgage Status','Remark'];

    var tableData = [
      [8800 ,'Ramdev S/O Hariram', 3, 4, 5, 6],
      ['a', 'b', 'c', 'd', 'e', 'f'],
    ];
    const widthArr = [80, 150, 100, 80, 80, 80];


    return (

      <Container style={styles.container}>
        
        <Content style={styles.section}>
          
          <View style={{marginBottom:20}}>
          <Text style={{fontWeight:'600',margin:15,fontSize:18,color:'#777'}}>
            View Mortgage
          </Text>
            <List>
            <ListItem>
                  <Body>  
                    <Text>Customer Code : JGD-5</Text>
                  </Body>
            </ListItem>
            <ListItem>
                  <Body>
                    <Text>Mobile : 8802469039</Text>
                  </Body>
            </ListItem>
            <ListItem>
                  <Body>
                    <Text>Borrow Exists : 
                    
                    </Text>
                  </Body>
                  <Right>
                  {this.state.userdata.Customer.PendingBorrowExists && <Badge info><Text>Yes</Text></Badge>}
                  {!this.state.userdata.Customer.PendingBorrowExists && <Badge danger><Text>No</Text></Badge>}
                  </Right>
            </ListItem>
            <ListItem>
                  <Body>
                    <Text>Unsecured Mortgage Exists : 
                    
                    </Text>
                  </Body>
                  <Right>
                  {this.state.userdata.Customer.InSecMrtgExists && <Badge info><Text>Yes</Text></Badge>}
                  {!this.state.userdata.Customer.InSecMrtgExists && <Badge danger><Text>No</Text></Badge>}
                  </Right>
            </ListItem>
            <ListItem>
                  <Body>
                    
                    <Text>Address : Delhi</Text>
                  </Body>
            </ListItem>
            </List>
          </View>
          <View style={{marginLeft:15,marginBottom:5}}>
          <Text>Mortgage Details</Text>
          <Table style={styles.table}>
          <ScrollView horizontal={true} style={{borderColor:'#f60'}}>
            <TableWrapper>
              <Row data={tableHead} style={styles.head} textStyle={styles.headText} widthArr={widthArr}/>
              {
                tableData.map((data, i) => (
                  <Row key={i} data={data} style={[styles.list, i%2 && {backgroundColor: '#DFF5F2'}]} widthArr={widthArr} textStyle={styles.listText}/>
                ))
              }
            </TableWrapper>
          </ScrollView>
        </Table>
        </View>

        <View style={{marginLeft:15,marginBottom:5}}>
          <Text>Mortgage Details</Text>
          <Table style={styles.table}>
          <ScrollView horizontal={true} style={{borderColor:'#f60'}}>
            <TableWrapper>
              <Row data={tableHead} style={styles.head} textStyle={styles.headText} widthArr={widthArr}/>
              {
                tableData.map((data, i) => (
                  <Row key={i} data={data} style={[styles.list, i%2 && {backgroundColor: '#DFF5F2'}]} widthArr={widthArr} textStyle={styles.listText}/>
                ))
              }
            </TableWrapper>
          </ScrollView>
        </Table>
        </View>

        <View style={{marginLeft:15,marginBottom:5}}>
          <Text>Mortgage Details</Text>
          <Table style={styles.table}>
          <ScrollView horizontal={true} style={{borderColor:'#f60'}}>
            <TableWrapper>
              <Row data={tableHead} style={styles.head} textStyle={styles.headText} widthArr={widthArr}/>
              {
                tableData.map((data, i) => (
                  <Row key={i} data={data} style={[styles.list, i%2 && {backgroundColor: '#DFF5F2'}]} widthArr={widthArr} textStyle={styles.listText}/>
                ))
              }
            </TableWrapper>
          </ScrollView>
        </Table>
        </View>

        <View style={{marginLeft:15,marginBottom:5}}>
          <Text>Mortgage Details</Text>
          <Table style={styles.table}>
          <ScrollView horizontal={true} style={{borderColor:'#f60'}}>
            <TableWrapper>
              <Row data={tableHead} style={styles.head} textStyle={styles.headText} widthArr={widthArr}/>
              {
                tableData.map((data, i) => (
                  <Row key={i} data={data} style={[styles.list, i%2 && {backgroundColor: '#DFF5F2'}]} widthArr={widthArr} textStyle={styles.listText}/>
                ))
              }
            </TableWrapper>
          </ScrollView>
        </Table>
        </View>
          <View>
          <Button block style={styles.btn}><Text>View Items</Text></Button>
          <Button block style={styles.btn}><Text>View Details</Text></Button>
          <Button block style={styles.btn}><Text>Deposit Payment</Text></Button>
          <Button block style={styles.btn}><Text>Update</Text></Button>
          <Button block style={styles.btn}><Text>Unmortgage</Text></Button>
          <Button block style={styles.btn}><Text>Add Additional</Text></Button>
          <Button block style={styles.btn}><Text>Full Pay Additional</Text></Button>
          <Button block success style={styles.btn}><Text>Done</Text></Button>
          </View>

          <View>
          <Text>
            { JSON.stringify(this.state.userdata.Mortgage) }
          </Text>
          </View>
          
        </Content>
        
      </Container>


    )
    
  
    
  }

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2',
  },
  section:{
    margin:10,
    backgroundColor:'#fff',
    flex:1,
    paddingBottom:10,
    paddingRight:15,
    flexDirection:'column',

  },
  btn:{
    marginBottom:5,
    marginLeft:15,
  },
  table: { width: '100%', flexDirection: 'row',marginTop:5 },
  head: { backgroundColor: '#555', height: 30 },
  headText: { color: '#fff', textAlign: 'center',fontSize:13 },
  titleText: { marginLeft: 6 },
  list: { height: 28, backgroundColor: '#f0f0f0' },
  listText: { textAlign: 'center', marginRight: 0 },
});

