import React from 'react';
import { Platform, ScrollView, StyleSheet, Text,Dimensions,View,Note } from 'react-native';
import { Container, Header, Content, Form, Item,Body,Spinner, Input, Label,Button,List,ListItem,Right,Left,Radio,Picker } from 'native-base';
import api from '../../utils/api.js';

export default class FindMortgage extends React.Component {
  static navigationOptions = {
    title: 'Find Mortgage',
  };

  findmortgage(){
    this.setState({showspinner:true});
    api.findMortgage(this.state)
    .then((res) => res.json()
    .then((responseJson) => {
      console.log(responseJson);
      if(responseJson.Status == 1){
        this.setState({listofmortgages:responseJson.ReturnData});
        this.setState({showspinner:false});
        this.setState({showresults:true});
      }
      else{alert("Error! Something went wrong");}
    })
    );
  }

  viewmortgage(value1,value2){
    api.findSingleMortgage(value1,value2)
    .then((res) => res.json()
    .then((responseJson) => {
      if(responseJson.Status == 1){this.gotomortgage(responseJson);}
      else{alert("Error! Something went wrong");}
    })
    );
  }
  gotomortgage(responseJson){
    this.props.navigation.navigate("View Mortgage",responseJson);
  }

   constructor(){
    super();
    this.state = {
      fname:'',
      lname:'',
      mobile:'',
      city: '',
      customercode:'',
      listofmortgages:[],
      showspinner:false,
      showresults : false,
    }
  }

  reset(){
    this.setState({
      fname:'',
      lname:'',
      mobile:'',
      city: '',
      customercode:'',
      listofmortgages:[],
      showspinner:false,
      showresults : false,
      cvdata:cv,
    });
  }
  
 citychange(value: string) {
    this.setState({
      city: value
    });
  }
  monthchange(value: string) {
    this.setState({
      month: value
    });
  }
  yearchange(value: string) {
    this.setState({
      year: value
    });
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <Content style={styles.section}>
          <Text style={{fontWeight:'600',margin:15,fontSize:18,color:'#777'}}>
            Find Mortgage
          </Text>
          <Form>
            <Item stackedLabel>
              <Label>First Name</Label>
              <Input onChangeText={(text) => this.setState({fname: text})} value={this.state.fname}/>
            </Item>
            <Item stackedLabel>
              <Label>Last Name</Label>
              <Input onChangeText={(text) => this.setState({lname: text})} value={this.state.lname}/>
            </Item>
            <Item stackedLabel>
              <Label>Mobile Number</Label>
              <Input onChangeText={(text) => this.setState({mobile: text})} value={this.state.mobile}/>
            </Item>
            <Item stackedLabel>
              <Label>Village/City</Label>
              <Picker style={{ width:(Platform.OS === 'ios') ? undefined : '100%',flexDirection:'row',alignItems:'flex-start' }}
                mode="dropdown"
                placeholder="Select One"
                selectedValue={this.state.city}
                onValueChange={this.citychange.bind(this)}
              >
                <Item label="city 1" value="2"/>
                <Item label="city 2" value="3"/>
                <Item label="city 3" value="4"/>
              </Picker>
            </Item>

            <Item stackedLabel>
              <Label>Customer Code</Label>
              <Input onChangeText={(text) => this.setState({customercode: text})} value={this.state.customercode}/>
            </Item>

            <Item stackedLabel>
              <Label>Mortgage TR No.</Label>
              <Input onChangeText={(text) => this.setState({customercode: text})} value={this.state.customercode}/>
            </Item>

            <Item stackedLabel>
              <Label>Principal Amount</Label>
              <Input onChangeText={(text) => this.setState({customercode: text})} value={this.state.customercode}/>
            </Item>

            <Item stackedLabel>
              <Label>Month</Label>
              <Picker style={{ width:(Platform.OS === 'ios') ? undefined : '100%',flexDirection:'row',alignItems:'flex-start' }}
                mode="dropdown"
                placeholder="Select One"
                selectedValue={this.state.month}
                onValueChange={this.monthchange.bind(this)}
              >
                <Item label="Jan-1" value="Jan-1"/>
                <Item label="Feb-2" value="Feb-2"/>
                <Item label="Mar-3" value="Mar-3"/>
              </Picker>
            </Item>

            <Item stackedLabel>
              <Label>Year</Label>
              <Picker style={{ width:(Platform.OS === 'ios') ? undefined : '100%',flexDirection:'row',alignItems:'flex-start' }}
                mode="dropdown"
                placeholder="Select One"
                selectedValue={this.state.year}
                onValueChange={this.yearchange.bind(this)}
              >
                <Item label="2017-2018" value="2017-2018"/>
                <Item label="2018-2019" value="2018-2019"/>
              </Picker>
            </Item>
            
            <View style={{flexDirection:'row',marginTop:10,marginLeft:15}}>
            <Button primary block style={{flex:1,marginRight:5}} onPress={this.findmortgage.bind(this)}><Text> Find Mortgage </Text></Button>
            <Button light block style={{flex:1,marginLeft:5}}><Text> Clear </Text></Button>
            </View>
          </Form>
        </Content>


        {this.state.showspinner && <Spinner />}

        {this.state.showresults && <Content style={styles.section}>
          <Text style={{fontWeight:'600',margin:15,fontSize:18,color:'#777'}}>
            Results
          </Text>
          
          <List
            dataArray={this.state.listofmortgages}
            renderRow={data => {
              return (
                <ListItem>
                <Body>
                  <Text>{data.CustFName} {data.CustLName}</Text>
                  <Text>S/O {data.GardFName} {data.GardLName}</Text>
                  <Text>{data.AddressName}</Text>
                </Body>
                <Right>
                  <Button small bordered onPress={this.viewmortgage.bind(this,data.SecCustCodeId,data.SecMrtgCodeId)}><Text>  View  </Text></Button>
                </Right>
                  
                </ListItem>
              );
            }}
          />
        </Content>
      }
      </ScrollView>


    )
    
  
    
  }

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2',
  },
  section:{
    margin:10,
    backgroundColor:'#fff',
    flex:1,
    paddingBottom:10,
    paddingRight:15,
  },
});

