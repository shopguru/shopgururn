import React from 'react'; 
import { Platform, ScrollView, StyleSheet, Text,Dimensions,View } from 'react-native';
import { Container, Header, Content, Form, Item,Body, Input, Label,Button,List,ListItem,Right,Left,Radio,PickerIcon } from 'native-base';
import api from '../../utils/api.js';
import DatePicker from 'react-native-datepicker';
import Modal from "react-native-modal";

export default class AddMortgage extends React.Component {
  static navigationOptions = {
     title: 'Add Mortgage',
  };

  constructor(props){
    super(props)
    this.state = {
      date:"2016-05-15",
      isModalVisible:false,
    }
  }

 _toggleModal = () =>
    this.setState({ isModalVisible: !this.state.isModalVisible });

  /*addcustomer(){
    api.addCustomer(this.state)
    .then((res) => res.json()
    .then((responseJson) => {
      console.log(responseJson);
      if(responseJson.Status == 1){this.showsuccess();}
      else{alert("Error! Something went wrong");}
    })
    );
  }*/

  /*constructor(){
    super();
    this.state = {
      fname:'',
      lname:'',
      gender:'',
      mobile:'',
      gfname:'',
      glname:'',
      relation:'',
      profession:'',
      idno:'',
      idtype:'',
      city:'',
      district:'',
      address:'',
      mystate:'',
      contact:'',
    }
  }*/
  /*reset(){
    this.setState({
      fname:'',
      lname:'',
      gender:'',
      mobile:'',
      gfname:'',
      glname:'',
      relation:'',
      profession:'',
      idno:'',
      idtype:'',
      city:'',
      district:'',
      address:'',
      mystate:'',
      contact:'',
    })
  }*/
  /*relationchange(value: string) {
    this.setState({
      relation: value
    });
  }
  professionchange(value: string) {
    this.setState({
      profession: value
    });
  }
  idtypechange(value: string) {
    this.setState({
      idtype: value
    });
  }
  districtchange(value: string) {
    this.setState({
      district: value
    });
  }
  citychange(value: string) {
    this.setState({
      city: value
    });
  }*/
  typechange(value: string) {
    this.setState({
      type: value
    });
  }
  additem(){

    this._toggleModal();
  }
  /*showsuccess(){
    this.props.navigation.navigate("Add Customer Success");
  }*/
  render() {
    return (
      <ScrollView style={styles.container}>
        <Content style={styles.section}>
          <Text style={{fontWeight:'600',margin:15,fontSize:18,color:'#777'}}>
            Add Mortgage
          </Text>
          <View style={{marginLeft:15,padding:5,backgroundColor:'#f5f5f5'}}>
            <Text style={styles.txt}>Customer Code : JGD-1</Text>
            <Text style={styles.txt}>Mobile : 9811223344</Text>
            <Text style={styles.txt}>Ramdev S/O|D/O Kamdev</Text>
          </View>
          <Form>
            <Item stackedLabel>
              <Label>Principal</Label>
              <Input onChangeText={(text) => this.setState({principal: text})} value={this.state.principal}/>
            </Item>
            <Item stackedLabel>
              <Label>Date</Label>
            <DatePicker
              style={{width:'100%',marginTop:10,marginBottom:5,borderWidth:0}}
              date={this.state.date}
              mode="date"
              placeholder="select date"
              format="YYYY-MM-DD"
              minDate="2016-05-01"
              maxDate="2016-06-01"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  right: 0,
                  top: 4,
                  marginLeft: 0
                },
                dateInput: {
                  marginLeft: 0
                } 
              }}
              onDateChange={(date) => {this.setState({date: date})}}
            />
            </Item>
            <Item stackedLabel>
              <Label>Interest Rate</Label>
              <Input onChangeText={(text) => this.setState({interest: text})} value={this.state.interest}/>
            </Item>
            <Item stackedLabel>
              <Label>Commited Months</Label>
              <Input onChangeText={(text) => this.setState({commitedmonths: text})} value={this.state.commitedmonths}/>
            </Item>
            <View style={{marginTop:10,marginLeft:15}}>
            <Button small bordered onPress={this._toggleModal}>
            <Text>  Add Item  </Text>
            </Button>

            
            </View>
            <List>
              <ListItem style={{backgroundColor:'#f5f5f5',paddingLeft:5}}>
               <Body>
               <Text>Qty - 1</Text>
               <Text>Item - Item 1</Text>
               <Text>Type - Type 1</Text>
               <Text>Weight - 20kg</Text>
               </Body>
               <Right>
                <Button small bordered><Text> Delete </Text></Button>
               </Right>
              </ListItem>
            </List>
            <Item stackedLabel>
              <Label>Remarks</Label>
              <Input onChangeText={(text) => this.setState({interest: text})} value={this.state.interest}/>
            </Item>

          <Modal isVisible={this.state.isModalVisible} style={{flex:1,backgroundColor:'#fff',borderRadius:2,margin:30}}>
          <View style={{ flex: 1,flexDirection:'column',padding:5,marginRight:15 }}>
            <Text style={{fontWeight:'600',margin:15,fontSize:18,color:'#777'}}>
            Add Items
            </Text>
            
            
              
              <Item>
              <Input placeholder='Enter Quantity' onChangeText={(text) => this.setState({qty: text})} value={this.state.qty}/>
              </Item>
              <Item>
              <Input placeholder='Enter Item' onChangeText={(text) => this.setState({qty: text})} value={this.state.qty}/>
              </Item>
              <Item>
              <DatePicker style={{ width:(Platform.OS === 'ios') ? undefined : '100%',flexDirection:'row',alignItems:'flex-start' }}
                mode="dropdown"
                placeholder="Select Type"
                selectedValue={this.state.type}
                onValueChange={this.typechange.bind(this)}
              >
                <Item label="Type 1" value="2"/>
                <Item label="Type 2" value="3"/>
                <Item label="Type 3" value="4"/>
              </DatePicker>
              </Item>
              <Item>
              <Input placeholder='Enter Weight' onChangeText={(text) => this.setState({weight: text})} value={this.state.weight}/>
              </Item>
              <View style={{marginTop:10,marginLeft:15}}>
                <Button block onPress={this.additem.bind(this)}>
                  <Text>Save</Text>
                </Button>
                <Button block light style={{marginTop:10}} onPress={this._toggleModal}>
                  <Text>Cancel</Text>
                </Button>
              </View>
            
            
          </View>
          </Modal>

            
            

            <View style={{flexDirection:'row',marginTop:10,marginLeft:15}}>
            <Button primary block style={{flex:1,marginRight:5}} ><Text> Save </Text></Button>
            <Button light block style={{flex:1,marginLeft:5}} ><Text> Cancel </Text></Button>
            </View>
          </Form>
        </Content>
      </ScrollView>
    );
  }
}

  
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2',
  },
  section:{
    margin:10,
    backgroundColor:'#fff',
    flex:1,
    paddingBottom:10,
    paddingRight:15,
  },
  txt:{
    fontSize:13,
  },
});
